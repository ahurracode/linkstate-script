#! /usr/bin/env python3

#Importing modules
import subprocess
import re
import time

# 
#Python script to demonstrate dynamic routing over multiple links using Linux 
#




#Initializing variables
state = None
Num=0

print('>>>>>>>>GROUP B LINK MONITOR<<<<<<<<')
print('            netLabsUg')
print ('*********************************')

#Acquiring hostname 
rtr_name= str(subprocess.check_output("/bin/hostname",shell=True))
rtr_match = re.findall(r'.*(n\d).*',rtr_name)

if (rtr_match ==['n2']):
    MTN_IP='10.10.10.2'
    UTL_IP='10.10.10.6'
    WLAN_IP='10.10.10.14'
elif (rtr_match ==['n3']):
    MTN_IP='10.10.10.1'
    UTL_IP='10.10.10.5'
    WLAN_IP='10.10.10.9'
else:
    print (rtr_match[0],' is not of the Link routers')


while 1:
# Test links

    time.sleep(5)
    MTN_test = subprocess.call(['fping',MTN_IP])
    UTL_test = subprocess.call(['fping',UTL_IP])
    WLAN_test = subprocess.call(['fping',WLAN_IP])

    if (state==None):
        print ('--->Determining Links')
# State 0

    if (MTN_test==0 and UTL_test==0 and WLAN_test==0):
        if (state == 0):
            print("---> State 0 : All links are up")
        else:
            Num= int(subprocess.check_output('(/sbin/ip route list | grep "default" | wc -l)',shell=True))
            while (Num>0):
                subprocess.call('/sbin/ip route del default',shell=True)
                Num-=1
       
            subprocess.call('/sbin/ip route add default nexthop via '+MTN_IP+' weight 1 nexthop via '+UTL_IP+' weight 1',shell=True)
            print("---> Added multi-path route via both MTN and UTL")
            print("---> State 0 : All links are up")
            state=0



 # State 1 

    elif (MTN_test == 0 and UTL_test != 0 and WLAN_test==0):
        if (state == 1):
            print("---> State 1 : MTN up,WLAN up, UTL down, Monitoring")
        else:
            Num=int(subprocess.check_output('(/sbin/ip route list | grep "default" | wc -l)',shell=True))
            while (Num>0):
                subprocess.call('/sbin/ip route del default',shell=True)
                Num-=1
            subprocess.call('/sbin/ip route add default nexthop via '+MTN_IP+' weight 1 nexthop via '+WLAN_IP+' weight 1',shell=True)
            print("---> Added multi-path route via both MTN and WLAN")
            print("---> State 1 : MTN up,WLAN up, UTL down, Monitoring")
            state=1
    

 # State 2 
    elif (MTN_test != 0 and UTL_test == 0 and WLAN_test==0):
        if (state == 2):
            print("---> State 2 : UTL up,WLAN up, MTN down, Monitoring")
        else:
            Num=int(subprocess.check_output('(/sbin/ip route list | grep "default" | wc -l)',shell=True))
            while(Num>0):
                subprocess.call('/sbin/ip route del default',shell=True)
                Num-=1
           
            subprocess.call('/sbin/ip route add default nexthop via '+WLAN_IP+' weight 1 nexthop via '+UTL_IP+' weight 1',shell=True)
            print("---> Added multi-path route via both UTL and WLAN")
            print("---> State 2 : UTL up,WLAN up, MTN down, Monitoring")
            state=2

 # State 3 
    elif (MTN_test != 0 and UTL_test != 0 and WLAN_test==0):
        if (state == 3):
            print("---> State 3 : WLAN up, UTL down, MTN down, Monitoring")
        else:
            Num=int(subprocess.check_output('(/sbin/ip route list | grep "default" | wc -l)',shell=True))
            while(Num>0):
                subprocess.call('/sbin/ip route del default',shell=True)
                Num-=1
           
            subprocess.call('/sbin/ip route add default via '+WLAN_IP+'' ,shell=True)
            print("---> Added bias route via  WLAN")
            print("---> State 3 : WLAN up, UTL down, MTN down, Monitoring")
            state=3
#State 4

    elif (MTN_test == 0 and UTL_test == 0 and WLAN_test!=0):
        if (state == 4):
            print("---> State 4 : UTL up, MTN up, WLAN down,  Monitoring")
        else:
            Num=int(subprocess.check_output('(/sbin/ip route list | grep "default" | wc -l)',shell=True))
            while(Num>0):
                subprocess.call('/sbin/ip route del default',shell=True)
                Num-=1
           
            subprocess.call('/sbin/ip route add default nexthop via '+MTN_IP+' weight 1 nexthop via '+UTL_IP+' weight 1',shell=True)
            print("---> Added multi-path route via both UTL and MTN")
            print("---> State 4 : UTL up, MTN up, WLAN down ,Monitoring")
            state=4


#State 5

    elif (MTN_test == 0 and UTL_test != 0 and WLAN_test!=0):
        if (state == 5):
            print("---> State 5 : MTN up, UTL down, WLAN down,Monitoring")
        else:
            Num=int(subprocess.check_output('(/sbin/ip route list | grep "default" | wc -l)',shell=True))
            while(Num>0):
                subprocess.call('/sbin/ip route del default',shell=True)
                Num-=1
           
            subprocess.call('/sbin/ip route add default via '+MTN_IP+'',shell=True)
            print("---> Added bias-path route via  MTN")
            print("---> State 5 : MTN up, UTL down,  WLAN down ,Monitoring")
            state=5
#State 6

    elif (MTN_test != 0 and UTL_test == 0 and WLAN_test!=0):
        if (state == 6):
            print("---> State 6 : UTL up, MTN down,  WLAN down, Monitoring")
        else:
            Num=int(subprocess.check_output('(/sbin/ip route list | grep "default" | wc -l)',shell=True))
            while(Num>0):
                subprocess.call('/sbin/ip route del default',shell=True)
                Num-=1
           
            subprocess.call('/sbin/ip route add default via '+UTL_IP+'',shell=True)
            print("---> Added bias-path route via  UTL")
            print("---> State 6 : UTL up, MTN down,  WLAN down, Monitoring")
            state=6
#State 7 

    elif (MTN_test != 0 and UTL_test != 0 and WLAN_test!=0):
        if (state == 7):
            print("---> State 7 : All links are down, panic !!!!  ,Monitoring")
        else:
            Num=int(subprocess.check_output('(/sbin/ip route list | grep "default" | wc -l)',shell=True))
            while(Num>0):
                subprocess.call('/sbin/ip route del default',shell=True)
                Num-=1
           
            print("---> Trying to add links")
            print("---> State 7 : All links are down, Panic !!!!  ,Monitoring")
            state=7



    
#end

